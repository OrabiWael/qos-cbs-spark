/**
 * Created by omarkayali on 6/25/16.
 */
public interface Policy {
    public double estimate(int state, double actualTime, int quality, Application [] apps);
}
