public class SimplePolicy implements Policy {

    @Override
    public double estimate(int state, double actualTime, int quality, Application[] apps) {

        Policy p = new SafetyPolicy();
        double estimationSafe = p.estimate(state, actualTime, quality, apps);
        double estimationAvr = 0;

        for (int i = state; i < apps.length; i++)
        {
            Application nextApp = apps[i];
            estimationAvr += nextApp.qualities[quality].getAverageCase();
        }

        return Math.max(estimationSafe, estimationAvr);
    }
}