import sun.dc.pr.PRError;

import java.io.*;


public class Application {

    public Quality [] qualities;
    private String name;
    private int currentQuality;
    private int currentNumberOfIterations;



    private File file;

    Application(File name)
    {
        this.setFile(name);
        this.setName(name.getName());
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void initialiazeQuality(int n)
    {
            qualities = new Quality[n];
    }

    public void setQualities(int qual, int numberOfIterations, double averageCase, double worstCase)
    {

            qualities[qual] = new Quality(numberOfIterations,worstCase,averageCase);
    }

    public long run( int quality) throws IOException
    {
        currentQuality = quality;
        long start = System.currentTimeMillis();
        long executionTime = 0;
        currentNumberOfIterations = qualities[quality].getNbOfIterations();

        //System.out.println(this.getFile().getPath());
        Process p = Runtime.getRuntime().exec("spark-submit "+ this.getFile().getPath()+" "+currentNumberOfIterations);

        printInfo();

        try
        {
            p.waitFor();
            long finish = System.currentTimeMillis();
            executionTime = (finish - start) / 1000;

        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        System.out.println("Execution Time: "+executionTime+" s\n");

        return executionTime;
    }

    public void printInfo()
    {
        System.out.println("Application Name: "+ getName());
        System.out.println("Quality Selected: "+(currentQuality+1));
        System.out.println("Number of Iterations: "+ currentNumberOfIterations);

    }
}
