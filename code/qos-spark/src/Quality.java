public class Quality {

    private int nbOfIterations;
    private double worstCase;
    private double averageCase;

    Quality(int nbOfIterations, double worstCase, double averageCase)
    {
        this.setNbOfIterations(nbOfIterations);
        this.setWorstCase(worstCase);
        this.setAverageCase(averageCase);
    }

    public int getNbOfIterations()
    {
        return nbOfIterations;
    }

    public void setNbOfIterations(int nbOfIterations)
    {
        this.nbOfIterations = nbOfIterations;
    }

    public double getWorstCase()
    {
        return worstCase;
    }

    public void setWorstCase(double worstCase)
    {
        this.worstCase = worstCase;
    }

    public double getAverageCase()
    {
        return averageCase;
    }

    public void setAverageCase(double averageCase)
    {
        this.averageCase = averageCase;
    }
}
