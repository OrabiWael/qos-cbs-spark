public class MixedPolicy implements Policy {

	@Override
	public double estimate(int state, double actualTime, int quality, Application [] apps)
	{
		double maxEstimation = 0;

		for(int i = state; i < apps.length; i++)
		{
			Policy p = new SafetyPolicy();
			double estimationSafe = p.estimate(i,actualTime,quality,apps);
			double  estimationAvr = 0;

			for(int j = state; j < i; j++)
			{
				Application nextApp = apps[j];
				estimationAvr += nextApp.qualities[quality].getAverageCase();
			}

			double curentEstimation = estimationSafe + estimationAvr;

			if(maxEstimation < curentEstimation)
				maxEstimation = curentEstimation;
		}
		return maxEstimation;
	}
}


/*
* max{
*
*	Csf(ai ..... an,q),
*
*	Cav(ai, q) +  Csf(ai+1 ..... an, q)
*
*	Cav(ai ai+1 , q) +  Csf(ai+2 ..... an, q)
*
*   .....
*
* 	Cav(ai .... an,q)
*
*   }
*
* */