public class SafetyPolicy implements Policy {

    @Override
    public double estimate(int state, double actualTime, int quality, Application[] apps)
    {

        int estimationTime = 0;
        Application currentApp = apps[state];
        estimationTime += currentApp.qualities[quality].getWorstCase();

        for (int i = state + 1; i < apps.length; i++)
        {
            Application nextApp = apps[i];
            estimationTime += nextApp.qualities[0].getWorstCase();
        }

        return estimationTime;
    }
}
