import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import java.util.*;

public class Controller {

	private double deadline;
	private String policyType;
	List<String> appSequence = new ArrayList<String>();
	public Application [] apps;

	public double getDeadline()
	{
		return deadline;
	}

	public int selectQuality(Policy policy, int state, double actualTime)
	{
		for(int quality = apps[state].qualities.length-1; quality >= 0; quality--)
		{
			double cX = policy.estimate(state, actualTime, quality, apps);
			//System.out.println((quality+1)+" "+cX);
			if(deadline - cX >= actualTime) return quality;
		}
		return 0;
	}


	public Controller(File input, File benchmark)
	{

		getInput(input);
		apps = new Application[appSequence.size()];
		for(int i = 0; i < apps.length; i++)
		{
			apps[i] = new Application(new File(appSequence.get(i)));
			getBenchmark(apps[i],benchmark);
		}
	}

	public static void main(String[] args) throws IOException {

		try {

			if(args.length == 0)
				throw new Exception("You should give the path of the INPUT file and the BENCHMARK file respectively");

			File input = new File(args[0]);
			File benchmark = new File(args[1]);

			String inputFileType = input.getName().substring(input.getName().length()-5);
			String benchmarkFileType = benchmark.getName().substring(benchmark.getName().length()-5);

			if(!inputFileType.equals(".json"))
				throw new Exception("The input file should be a JSON file ");
			else if(!benchmarkFileType.equals(".json"))
				throw new Exception("The benchmark file should be a JSON file ");

			Controller c = new Controller(input, benchmark);

			c.runQualityManager();

		}
		catch (Exception e)
		{
			System.err.println(e.getMessage());
		}

	}

	public Policy getPolicy()
	{
		if(policyType.equals("safe"))
		{
			System.out.println("Policy Selected: "+policyType+"\n");

			return new SafetyPolicy();
		}
		else if(policyType.equals("simple"))
		{
			System.out.println("Policy Selected: "+policyType+"\n");

			return new SimplePolicy();
		}
		else if(policyType.equals("mixed"))
		{
			System.out.println("Policy Selected: "+policyType+"\n");

			return new MixedPolicy();
		}

		return null;
	}

	public void runQualityManager()
	{

		System.out.println("Deadline: "+getDeadline()+"s\n");

		Policy policy = getPolicy();
		int actualTime = 0;

		for(int i = 0; i < appSequence.size(); i++)
		{
			int qual = selectQuality(policy,i,actualTime);

			try {

				actualTime += apps[i].run(qual);

			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		System.out.println("Total time "+actualTime);
	}

	public void getInput(File file) {

		deadline = 0.0;
		policyType = "";

		//Parse the json input data
		try
		{
			JSONParser parser = new JSONParser();
			JSONObject jsonObject = (JSONObject)parser.parse(new FileReader(file));

			JSONArray appSeq = (JSONArray) jsonObject.get("sequence");
			policyType = (String)jsonObject.get("policy");
			deadline = Double.parseDouble((String)jsonObject.get("deadline"));

			appSequence = new ArrayList<String>();

			for (int i=0; i<appSeq.size(); i++)
			{
				appSequence.add(appSeq.get(i).toString());
			}
		}
		catch (Exception e)
		{
			System.err.println("Error while parsing input file(json): " + e);
		}
	}

	public void getBenchmark(Application app,File file){

		//now parse the benchmark
		try
		{
			JSONParser parser = new JSONParser();
			JSONObject jsonObject = (JSONObject)parser.parse(new FileReader(file));

			int numberOfQualities = Integer.parseInt((String)jsonObject.get("numberOfQualities"));
			JSONObject appName = (JSONObject)jsonObject.get(app.getName());
			app.initialiazeQuality(numberOfQualities);

			for(int i=0; i < numberOfQualities; i++)
			{
				JSONObject qualityName = (JSONObject)appName.get("Q"+(i+1));
				double worstCase = Double.parseDouble((String)qualityName.get("worstCase"));
				double averageCase = Double.parseDouble((String)qualityName.get("averageCase"));
				int numberOfIterations = Integer.parseInt((String)qualityName.get("numberOfIterations"));
				app.setQualities(i,numberOfIterations,averageCase,worstCase);
			}
		}
		catch (Exception e) {
			System.err.println("error while parsing benchmark(json): " + e);
		}

	}
}


