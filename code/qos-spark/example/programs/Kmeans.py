from pyspark.mllib.clustering import KMeans, KMeansModel
from numpy import array
from math import sqrt
from pyspark import SparkContext
import sys
sc = SparkContext("local", "Kmeans App")


iter = int(sys.argv[1])

def error(point):
    center = clusters.centers[clusters.predict(point)]
    return sqrt(sum([x**2 for x in (point - center)]))


data = sc.textFile("hdfs://localhost:9000/mymTraining.txt")
parsedData = data.map(lambda line: array([float(x) for x in line.split(',')]))

clusters = KMeans.train(parsedData, 2, maxIterations=iter,
	    runs=10, initializationMode="random")
WSSSE = parsedData.map(lambda point: error(point)).reduce(lambda x, y: x + y)
print("Within Set Sum of Squared Error = " + str(WSSSE))
