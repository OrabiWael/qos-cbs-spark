from pyspark.mllib.classification import SVMWithSGD, SVMModel
from pyspark.mllib.regression import LabeledPoint
from pyspark import SparkContext
import math
import sys


def parsePoint(line):
    values = [float(x) for x in line.split(',')]
    return LabeledPoint(values[65], values[0:64])

def main():

    iter =  int(sys.argv[1])

    sc = SparkContext("local", "SVM App")
    data = sc.textFile("hdfs://localhost:9000/mymTraining2.txt")
    parsedData = data.map(parsePoint)

    model = SVMWithSGD.train(parsedData, iterations=iter)

    labelsAndPreds = parsedData.map(lambda p: (p.label, model.predict(p.features)))
    trainErr = labelsAndPreds.filter(lambda (v, p): v != p).count() / float(parsedData.count())
    print("Training Error = " + str(trainErr))



main()