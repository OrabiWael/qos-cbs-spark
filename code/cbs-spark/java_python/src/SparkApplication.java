public class SparkApplication
{
	private String name;
	private String path;
	private int number_of_inputs;
	private int number_of_outputs;

	public SparkApplication()
	{

	}
	public SparkApplication(String name, String path, int number_of_inputs, int number_of_outputs)
	{
		this.name = name;
		this.path = path;
		this.number_of_inputs = number_of_inputs;
		this.number_of_outputs = number_of_outputs;
	}
	public String getName()
	{
		return name;
	}
	public String getPath()
	{
		return path;
	}
	public int getNumberOfInputs()
	{
		return number_of_inputs;
	}
	public int getNumberOfOutputs()
	{
		return number_of_outputs;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public void setPath(String path)
	{
		this.path = path;
	}
	public void setNumberOfInputs(int number_of_inputs)
	{
		this.number_of_inputs = number_of_inputs;
	}
	public void setNumberOfOutputs(int number_of_outputs)
	{
		this.number_of_outputs = number_of_outputs;
	}
	public String toString()
	{
		return "Name: "+name+"\n Path: "+path+"\n Number of inputs: "+number_of_inputs+"\n Number of outputs: "+number_of_outputs;
	}

}