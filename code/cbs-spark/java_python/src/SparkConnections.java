import java.util.Arrays;

import org.ietf.jgss.Oid;

public class SparkConnections {
	private SparkApplication spark_app;
	private int number_of_inputs;
	private int number_of_outputs;
	private Link[] inputs;
	private Link[] outputs;

	public SparkConnections() {

	}

	public SparkConnections(SparkApplication spark_app, Link[] inputs,
			Link[] outputs) {
		this.spark_app = spark_app;
		this.number_of_inputs = this.spark_app.getNumberOfInputs();
		this.number_of_outputs = this.spark_app.getNumberOfOutputs();
		this.inputs = inputs;
		this.outputs = outputs;
	}

	public SparkApplication getSparkApplication() {
		return spark_app;
	}

	public int getNumberOfInputs() {
		return number_of_inputs;
	}

	public int getNumberOfOutputs() {
		return number_of_outputs;
	}

	public Link[] getInputs() {
		return inputs;
	}

	public Link[] getOutputs() {
		return outputs;
	}

	public void setSparkApplication(SparkApplication spark_app) {
		this.spark_app = spark_app;
	}

	public void setNumberOfInputs(int number_of_inputs) {
		this.number_of_inputs = number_of_inputs;
	}

	public void setNumberOfOutputs(int number_of_outputs) {
		this.number_of_outputs = number_of_outputs;
	}

	public void setInputs(Link[] inputs) {
		this.inputs = inputs;
	}

	public void setOutputs(Link[] outputs) {
		this.outputs = outputs;
	}
	
	public String toString(){
		return spark_app.toString()+", "+Arrays.toString(inputs)+", "+Arrays.toString(outputs);
	}
}