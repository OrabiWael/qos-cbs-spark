import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

public class Driver {

	/**
	 * @param args
	 * @throws FileNotFoundException
	 */
	static Set<Integer> vertices= new HashSet<Integer>();
	static String toFile="";
	static int edges=0;
	
	static ArrayList<Integer> G[];
	
	public static void main(String[] args) throws FileNotFoundException {
		File f = new File(args[0]);
		File f2 = new File(args[1]);
		List<SparkApplication> apps = readApplicationsInfo(f);
		List<SparkConnections> connections = readConnectionsInfo(f2, apps);
		System.out.println(connections);
		
		
		
		PrintWriter writer = new PrintWriter("specs.txt");

		for (SparkConnections sc : connections) {
			int nbr_of_inp = sc.getSparkApplication().getNumberOfInputs();
			int nbr_of_out = sc.getSparkApplication().getNumberOfOutputs();
			String toFile = "";

			toFile += sc.getSparkApplication().getPath() + ",";

			toFile += getOrder(connections, sc) + ",";
			toFile += nbr_of_inp + ",";
			toFile += nbr_of_out + ",";

			Link[] inputs = sc.getInputs();
			Link[] outputs = sc.getOutputs();

			for (int i = 0; i < nbr_of_inp; i++) {
				toFile += inputs[i] + ",";
			}

			for (int i = 0; i < nbr_of_out; i++) {
				toFile += outputs[i] + ",";
			}

			writer.println(toFile.substring(0, toFile.length() - 1));
		}

		writer.close();

		loopDetection();
	}

	//Parses the user defined input file containing application's info
	public static List<SparkApplication> readApplicationsInfo(File f)
			throws FileNotFoundException {
		List<SparkApplication> applications = new ArrayList<SparkApplication>();
		Scanner s = new Scanner(f);

		while (s.hasNextLine()) {
			SparkApplication app = new SparkApplication();

			app.setName(s.nextLine().split(":")[1]); //Name
			app.setPath(s.nextLine().split(":")[1]); //Path
			app.setNumberOfInputs(Integer.parseInt(s.nextLine().split(":")[1])); //Number of inputs
			app.setNumberOfOutputs(Integer.parseInt(s.nextLine().split(":")[1])); //Number of outputs
			
			applications.add(app);
		}

		s.close();
		return applications;
	}
	

	//Parses the user defined input file containing connection's info
	@SuppressWarnings("resource")
	public static List<SparkConnections> readConnectionsInfo(File f,
			List<SparkApplication> apps) throws FileNotFoundException,
			IllegalArgumentException {

		List<SparkConnections> connections = new ArrayList<SparkConnections>();

		for (SparkApplication app : apps) { //Building the list of Spark Connections
			SparkConnections sc = new SparkConnections(app,
					new Link[app.getNumberOfInputs()],
					new Link[app.getNumberOfOutputs()]);
			connections.add(sc);
		}

		Scanner s = new Scanner(f);
		File loopDetection= new File("loop.txt");
		PrintStream p = new PrintStream(loopDetection);
		
		while (s.hasNextLine()) {
			String line = s.nextLine();
			String name = line.split("\\.")[0];
			
			SparkConnections sc = getConnectionByAppName(connections, name);

			if (line.split("\\.")[1].startsWith("in")) { //If app.in is defined call fillInputs
				Link[] ins = sc.getInputs();
				ins = fillInput(apps, connections, ins, line);
			} else if (line.split("\\.")[1].startsWith("out")) {//If app.out is defined call fillOutputs
				Link[] outs = sc.getOutputs();
				outs = fillOutput(outs, line);
			} else {
				throw new IllegalArgumentException("Wrong argument");
			}

		}
		p.println(vertices.size()+1+" "+edges);
		p.print(toFile);
		
		s.close();
		return connections;
	}

	private static Link[] fillOutput(Link[] outs, String line) {

		int index = Integer.parseInt(line.split("\\.")[1].substring(3).split(
				":")[0]);
		Link l = new Link();

		String path = line.split(":")[1];
		l.setPath(path);

		outs[index - 1] = l;
		return outs;
	}

	private static Link[] fillInput(List<SparkApplication> apps,
			List<SparkConnections> connections, Link[] ins, String line) {
		int index = Integer.parseInt(line.split("\\.")[1].substring(2).split(
				":")[0]);
		Link l = new Link();

		String path = line.split(":")[1];

		if (getAppByName(apps, path.split("\\.")[0]) == null) {//If the input is a file, set it
			l.setPath(path);
		} else { //Else if it is an output of another file
			
			String input= line.split(".in")[0];
			String output= path.split(".out")[0];
			
			int inNumber = Integer.parseInt(input.substring(input.length()-1)); //input number
			int outNumber = Integer.parseInt(output.substring(output.length()-1)); //output number
			
			vertices.add(inNumber);
			vertices.add(outNumber);
			edges++;
			
			toFile+= outNumber+" "+inNumber+"\n";
			
			int outIndex=Integer.parseInt(path.split(".out")[1]);
			Link out = getConnectionByAppName(connections,output).getOutputs()[outIndex - 1];

			l.setPath(out.getPath());
		}

		ins[index - 1] = l;
		return ins;
	}

	private static SparkConnections getConnectionByAppName(
			List<SparkConnections> connections, String name) {
		SparkConnections ret = null;
		for (SparkConnections c : connections) {
			if (c.getSparkApplication().getName().equals(name))
				ret = c;
		}
		return ret;
	}

	private static SparkApplication getAppByName(List<SparkApplication> apps,
			String name) {
		SparkApplication ret = null;
		for (SparkApplication a : apps) {
			if (a.getName().equals(name))
				ret = a;
		}
		return ret;
	}
	
	
	private static void loopDetection() throws FileNotFoundException{
		Scanner scan = new Scanner(new File("loop.txt"));

		int V = scan.nextInt();		//number of vertices in the graph
		int E = scan.nextInt();		//number of edges in the graph
		G = new ArrayList[V];	   	//create array of lists...length = V

		for(int i = 0; i < V; i++)
			G[i] = new ArrayList<Integer>();	//initialize the lists

		//reading input
		for(int i = 0; i < E; i++)
		{
			int u = scan.nextInt();
			int v = scan.nextInt();

			G[u].add(v);
			//G[v].add(u);	//add this line only if graph is undirected
	
		}
		if(hasCycle(1)){
			System.out.println("The system has a loop");
		}
		else{
			System.out.println("The system is correct");
		}
		
		scan.close();
	}
	public static boolean hasCycle(int s)
	{
		//G.length is equal to V which is the number of vertices
		int color[] = new int[G.length];	//color = 0 means not visited --- color = 1 means in the queue --- color = 2 means explored

		Queue<Integer> q = new LinkedList<Integer>();

		q.offer(s);		//put the source vertex in the queue
		color[s] = 1;	//since it is in the queue, its' color becomes 1

		while(!q.isEmpty())
		{
			int u = q.poll();	//remove a vertex from the queue

			//for each neighbor of u in the graph G
			for(int i = 0; i < G[u].size(); i++)
			{
				int v = G[u].get(i);	//get the neighbor

				if(color[v] == 0)	//if the vertex is not visited
				{
					q.offer(v);		//put it in the queue
					color[v] = 1;	//color it 1
				}

				if(color[v] == 2)
					return true;	//if we reach an explored vertex, then we have a cycle
			}

			color[u] = 2;	//vertex u is completely explored now
		}

		return false;
	}
	
	public static int getOrder(List<SparkConnections> connections,
			SparkConnections job) {
		int order = 0; // by default order 0

		HashSet<String> allIns = new HashSet<String>();
		HashSet<String> allOuts = new HashSet<String>();

		for (SparkConnections sc : connections) {
			Link[] inputs = sc.getInputs();
			Link[] outputs = sc.getOutputs();

			for (int i = 0; i < inputs.length; i++) {
				allIns.add(inputs[i].getPath());
			}

			for (int i = 0; i < outputs.length; i++) {
				allOuts.add(outputs[i].getPath());
			}
		}

		boolean hasIn = false; // indirect in: coming from another subjob
		boolean hasOut = false;

		Link[] jobIns = job.getInputs();
		Link[] jobOuts = job.getOutputs();

		for (Link l : jobIns) { // This is to check if the in exits in the outs
			if (allOuts.contains(l.getPath())) {
				hasIn = true;
			}
		}

		for (Link l : jobOuts) { // This is to check if the out exits in the ins
			if (allIns.contains(l.getPath())) {
				hasOut = true;
			}
		}

		if (hasIn && hasOut) {
			order = 1;
		} else if (hasIn && !hasOut) {
			order = 2;
		} else if (!hasIn && !hasOut) {
			order = 3;
		}
		return order;
	}
}
