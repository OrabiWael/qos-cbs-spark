import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

public class Wrapper {
	public static void main(String[] args) throws IOException{

		ArrayList<SparkConnections> Subjobs = new ArrayList<SparkConnections>();
		
		SparkApplication sapp= new SparkApplication("S1", "Desktop:/s1.py", 2, 1);
		
		Link[] linksIn= {new Link("", false, "in1.txt"), new Link("Test", true, "in2.txt")};
		
		Link[] linksOut={new Link("", false, "out1.txt")};
		
		SparkConnections scon= new SparkConnections(sapp, linksIn, linksOut);
		Subjobs.add(scon);
		
		
		
		PrintWriter writer = new PrintWriter("specs.txt");
	
		for(SparkConnections sc: Subjobs){
			int nbr_of_inp= sc.getSparkApplication().getNumberOfInputs();
			int nbr_of_out= sc.getSparkApplication().getNumberOfOutputs();
			String toFile= "";
			
			toFile+= sc.getSparkApplication().getPath()+",";
			
			toFile+= getOrder(Subjobs,sc)+",";
			toFile+= nbr_of_inp+",";
			toFile+= nbr_of_out+",";
		
			Link[] inputs= sc.getInputs();
			Link[] outputs= sc.getOutputs();
			
			for(int i=0; i<nbr_of_inp; i++){
				toFile+= inputs[i]+",";
			}
			
			for(int i=0; i<nbr_of_out; i++){
				toFile+= outputs[i]+",";
			}
			
			writer.println(toFile.substring(0, toFile.length()-1));
		}
		
		writer.close();
		
		// We need to run our python script from java code
		//Process p = Runtime.getRuntime().exec("python test.py");
		String[] cmd = new String[2];
		cmd[0] = "python";
		cmd[1] = "SpecsGC.py";
		
		// create runtime to execute external command
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec(cmd);
 
		// retrieve output from python script
		BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String line = "";
		while((line = bfr.readLine()) != null) 
		{
			// display each output line form python script
			System.out.println(line);
		}
	}
	
	public static int getOrder(ArrayList<SparkConnections> Subjobs, SparkConnections job){
		int order=0; //by default order 0
		
		HashSet<String> allIns= new HashSet<String>();
		HashSet<String> allOuts= new HashSet<String>();
		
		for(SparkConnections sc: Subjobs){
			Link[] inputs= sc.getInputs();
			Link[] outputs= sc.getOutputs();
			
			for(int i=0; i<inputs.length; i++){
				allIns.add(inputs[i].getPath());
			}
			
			for(int i=0; i<outputs.length; i++){
				allOuts.add(outputs[i].getPath());
			}
		}
		
		boolean hasIn= false; //indirect in: coming from another subjob
		boolean hasOut= false;
		
		Link[] jobIns= job.getInputs();
		Link[] jobOuts= job.getOutputs();
		
		for(Link l: jobIns){ //This is to check if the in exits in the outs 
			if(allOuts.contains(l.getPath())){
				hasIn= true;
			}
		}
		
		for(Link l: jobOuts){ //This is to check if the out exits in the ins
			if(allIns.contains(l.getPath())){
				hasOut= true;
			}
		}
		
		if(hasIn && hasOut){
			order= 1;
		}
		else if(hasIn && !hasOut){
			order= 2;
		}
		else if(!hasIn && !hasOut){
			order= 3;
		}
		return order;
	}
	
	public List<SparkApplication> readApplicationsInfo(File f) throws FileNotFoundException{
		List<SparkApplication> applications= new ArrayList<SparkApplication>();
		Scanner s= new Scanner(f);
		
		while(s.hasNextLine()){
			
			if(s.nextLine().contains("job")){
				SparkApplication app = new SparkApplication();
				app.setName(s.nextLine().split(":")[1]);
				app.setPath(s.nextLine().split(":")[1]);
				app.setNumberOfInputs(Integer.parseInt(s.nextLine().split(":")[1]));
				app.setNumberOfOutputs(Integer.parseInt(s.nextLine().split(":")[1]));				
				
				applications.add(app);
			}
		}
		
		return applications;
	}
}