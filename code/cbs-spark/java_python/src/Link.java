public class Link {
	private String name; // In case the output is indirect, this is used to keep track of where does the input come
	private boolean direct; // true if direct -- false otherwise
	private String path;

	public Link() {

	}

	public Link(String name, boolean direct, String path) {
		this.name = name;
		this.direct = direct;
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public boolean getDirect() {
		return direct;
	}

	public String getPath() {
		return path;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDirect(boolean direct) {
		this.direct = direct;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String toString() {
		return path;
	}
}