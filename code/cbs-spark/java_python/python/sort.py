from __future__ import print_function

import sys

from pyspark import SparkContext


if __name__ == "__main__":
    for i in range(4, 7):
        out = i
        inp = i-3
        target = open(sys.argv[out],"w")
      #  if len(sys.argv) != 3:
     #       print("Usage: sort <file>", file=sys.stderr)
      #      exit(-1)
        sc = SparkContext(appName="PythonSort")
        lines = sc.textFile(sys.argv[inp], 1)
        sortedCount = lines.flatMap(lambda x: x.split(' ')) \
            .map(lambda x: ((x), 1)) \
            .sortByKey()
        # This is just a demo on how to bring all the sorted data back to a single node.
        # In reality, we wouldn't want to collect all the data to the driver node.
        output = sortedCount.collect()
        for (num, unitcount) in output:
            target.write(num)
            target.write(" ")
            target.write("\n")
        sc.stop()