from __future__ import print_function

import sys
from operator import add

from pyspark import SparkContext


if __name__ == "__main__":
	myfile = open(sys.argv[1],"r")
	if len(sys.argv) != 2:
	        print("Usage: wordcount <file>", file=sys.stderr)
	        exit(-1)
	for line in myfile:
		print ("Line: ",line)
