#!/usr/bin/python
from __future__ import print_function
import thread
import sys

import socket   

import subprocess


class SubJob(object):
	
	def __init__(self, path, order, inputs, outputs, port):
		self.path = path
		self.order = order
		self.inputs= inputs
		self.outputs= outputs
		self.port= port

	def toString(self):
		jobstr= self.path[0:-3]+"_GC"+sub.order+".py "+self.path
		for inpts in self.inputs:
			jobstr= jobstr+" "+inpts
		for oupts in self.outputs:
			jobstr = jobstr + " " + oupts

		return jobstr[0:-1]

	def printSJ(self):
		print(self.path)
		print(str(self.order))
		print(self.port)
		i= 0
		print (len(self.inputs))
		while i< len(self.inputs):
			i= i+1
		i= 0
		print(len(self.outputs))
		while i< len(self.outputs):
			i= i+1
	
	def createGc(self):
		files= open(self.path[0:-3]+"_GC"+self.order+".py", "w")
		self.codeGeneration(files, self.port)

	def cg1_inp(self, file):
		cg1 = open("cgintro.txt", "r")  # intro to every subjob
		for line in cg1:
			file.write(line)

	def cg2_inp(self, file, port):
		cg2 = open("cgserver.txt", "r")  # creation of the server socket given the port number
		i = 0
		for line in cg2:
			if (i == 2):
				file.write("\t" + line[0:-1] + " " + str(port) + "\n")
			else:
				file.write("\t" + line)
			i = i + 1

	def cg3_inp(self, file, port):
		cg3 = open("cginfo.txt", "r")  # receiving input and output files
		for line in cg3:
			file.write("\t" + line)
		file.write("\n")

	def cg4_inp(self, file):
		cg4 = open("cgsubcall.txt", "r")  # calling on the subjob
		i = 0
		for line in cg4:
			file.write("\t" + line)
			i = i + 1

	def cg5_inp(self, file):
		cg5 = open("cgsend.txt", "r")  # send output
		i = 0
		for line in cg5:
			file.write("\t" + line)
			i = i + 1

	def getTime(self, file):
		file.write("\n\tt2= time.time()-t1\n")
		file.write("\tprint(\"time taken: ------->\"+str(t2))\n")
		file.write("\ttiming= file(\"timing.txt\", \"a\")\n")
		file.write("\ttiming.write(str(t2)+\"\\n\")")

	def codeGeneration(self, file, port):
		if(self.order=="0"):
			self.cg1_inp(file)
			self.cg2_inp(file, 12345)
			self.cg3_inp(file, port)
			self.cg4_inp(file)
			self.cg5_inp(file)
			self.getTime(file)

		elif(self.order=="1"):
			self.cg1_inp(file)
			self.cg2_inp(file, port)
			self.cg3_inp(file, port)
			self.cg4_inp(file)
			self.cg5_inp(file)
			self.getTime(file)

		elif(self.order=="2"):
			self.cg1_inp(file)
			self.cg2_inp(file, port)
			self.cg3_inp(file, port)
			self.cg4_inp(file)
			self.getTime(file)

		elif (self.order == "3"):
			self.cg1_inp(file)
			self.cg2_inp(file, 12345)
			self.cg3_inp(file, port)
			self.cg4_inp(file)
			self.getTime(file)

subjobs=[]

if __name__ == "__main__":
	soc = socket.socket()         # Create a socket object
	host = socket.gethostname() # Get local machine name
	starterPort = 12344             # Reserve a port for your service.
	soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	soc.bind((host, starterPort))        # Bind to the port
	soc.listen(1000)
	
	# Global variables
	global j
	global j1
	files = open("specs1.txt","r")

	i= 0
	port= starterPort
	for line in files:
		array = line.split(",")
		
		# Input files
		j = int (array[2])
		ins= []

		k = 4
		while k < j+4:
			ins.append(array[k])
			k = k + 1

		outs= []
		# Output files
		j1 = int (array[3])

		k1 = j + 4
		while k1 < 4 + j + j1:
			outs.append(array[k1])
			k1= k1 + 1
		path= array[0]
		order= array[1]
		sub= SubJob(path, order, ins, outs, port)


		present= False
		for s in subjobs:
			if path== s.path:
				present= True

		if(present):
			print("present "+path+" "+str(port))
		else:
			port= port+1
			print("not present " + path + " " + str(port))

		subjobs.append(sub)
		i= i+1

	i=0
	for sub in subjobs:
		SubJob.createGc(sub)
		print(sub.path+" "+str(i))
		i= i+1

	jobdata = ""
	for sub in subjobs:
		SubJob.createGc(sub)
		jobdata= jobdata+sub.toString()+"----"


	for sub in subjobs:

		if (sub.order == "0" or sub.order == "3"):
			gcrun=[]
			gcpath= sub.path[0:-3]+"_TestGC"+sub.order+".py"
			gcrun.append(gcpath)
			c, addr = soc.accept()
			c.send(jobdata)
