\section{Proposed Solution - Approach}
The proposed solution aims to integrate and relate several Spark sub-jobs into one relatively bigger sub-job dynamically based on some high level specifications. Each sub-job is considered as a black box where the embedded code and the functionality are unknown to us. The proposed system is independent from the functionality of each sub-job as long as they do not violate the predefined constraints. \newline 
Our approach is divided into three sections: High level input specification language, implementing synchronization, and code generation.

\subsection{Input Specification Language}
The input specification language is a high level one that defines the specifications that are extracted from an application specific description. This means that the user only needs to provide our system with the names of the sub-jobs and a description of the requirements of the application.\newline 
The following specifications should be provided to the system in order to produce the correct output:
\begin{enumerate}
	\item Name of each sub-job and its path
    \item Order of execution
    \item Number of inputs
    \item Number of outputs
    \item List of inputs
    \item List of outputs
    \item IP address of the host machine
    \item Port number of the sub-job process on the host machine
\end{enumerate}

\subsection{Synchronization}
The main challenge in our approach is the synchronization aspect between sub-jobs, maintaining the correct order of execution and increasing performance. As mentioned previously, there is a list of sub-jobs that are independent and can run in parallel. On the other hand, there exist some dependencies between some sub-jobs that should run sequentially. For example, suppose we have 3 sub-jobs named A,B, and C, if the input file of the sub-jobs B and C is the output file of the first sub-job A, then this dependency will force our system to execute A before B and C. B and C must run in parallel since they are independent, But A and (B,C) as a subsystem should be executed in sequence. \newline 
In order to solve the synchronization problem, we used \emph{Python} socket programming with blocking Send/Receive primitives. This means that if there exist some dependencies between some sub-jobs such as B is waiting an input file from A, then B should be blocked until A finishes its execution and sends the required information to B so it can starts its execution. Note that this is applicable on more generalized scenarios where the number of sub-jobs and dependencies is big. Figure 3 illustrates the solution of the synchronization problem between two sub-jobs A and B. \newline
\FloatBarrier
\begin{figure}[H]
	\input{tikz/fig3}
    \caption{Synchronization of A --> B}
\end{figure}
\subsection{Code Generation}
The code generation part of the proposed solution aims to generate code that will run the final system. This code will be generated dynamically based on the specifications and socket programming using blocking Send/Receive primitives. As mentioned earlier, if a sub-job named B is waiting for an input file from sub-job A, then its execution will be blocked until it gets its required input file. Figure 4 illustrates a scenario where B is waiting for its input from A. Note that this method is generalized in a way that the execution of the system is independent from the number of the sub-jobs.
Each two sub-jobs that need to communicate and send data to each other need to open a socket between them over a certain port number. \\

\iffalse
\FloatBarrier
\begin{figure}[H]
	\input{fig4}
    \caption{Code Generation of A and B sub-jobs}
\end{figure}
\fi

\begin{lstlisting}[style=Bash]
// Generated Code A' for a valid application A
$A.directInput1 = file\_path;
$A.directInput2 = file\_path;
Call(A);
Send(B',A.out);		
\end{lstlisting}

\begin{lstlisting}[style=Bash]
// Generated Code B' for a valid application B
Wait(A.out);
if(Receive(A.out);
	Call(B);
	Send(C',B.out); // if need be
\end{lstlisting}

After extracting the specifications indicated by the user explained in the previous subsection, a new executable python code will be generated for every group of sub-jobs having the same name and order. For example, for the sub-jobs \emph{subjob.py} having order 0, the generated code will be \emph{subjob\_GC0.py}. There might be another sub-jobs \emph{subjob .py} with order 1, so the generated code will be in this case \emph{subjob\_GC1.py}.\newline 
The order of the file is essential, since it determines the state at which the code generated will call the sub-jobs its linked to. We have four orders in our proposed solution.
\begin{enumerate}
	\item Order = 0: The generated code takes no input from any previous sub-jobs, but receives its needed input from \emph{specs\_GC.py}. That's why it blocks the execution until receiving the required inputs on a previously defined port number, that happens to be the port number of \emph{specs\_GC.py}. After calling on the corresponding sub-job, the resulting output gets sent over the socket for any waiting generated code.
    \item Order = 1: The generated code \emph{GC1.py} blocks the execution until receiving the required inputs over a socket from another generated code \emph{GC2.py} that falls in execution order before \emph{GC1.py} (extracted from the predefined specifications). After calling on the corresponding sub-job, the resulting output gets sent over another socket for any waiting generated code.
    \item Order = 2:  Just like a code generated with order 1, the generated code \emph{GC1.py} blocks the execution until receiving the required inputs over a socket from another generated code \emph{GC2.py} that falls in execution order before \emph{GC1.py}. After calling on the corresponding sub-job its output does not get sent to any other generated code, since a generated code with order 2 happens to be the last in the execution sequence.
    \item Order = 3: Receives input from \emph{specsGC.py} like that of order 0, and its output gets treated like that of order 2. A generated code of order 3 can be the first and the last node in the system at one of the branches.
\end{enumerate}
The code generated for every sub-job therefore depends on the order in the first place; each order-specific code has a certain inner built.  The general inner built consists of five chunks of codes that get merged in different ways to build the generated code based on its order that can be inferred from the input specifications and a given port number. Algorithm 1 is the pseudo code that defines the methods used to construct the newly generated code whereas algorithm 2 represents the way the methods defined in algorithm 1 get called and executed in the correct sequence based on the order of the corresponding sub-job.
\begin{algorithm}
\caption{Pseudo Code of each Procedure}\label{euclid}
\begin{algorithmic}[1]
\Procedure{cg\_Intro}{$\textit{filename}$}\ 
\State $\textit{Imports needed libraries for every generated code}$\
\EndProcedure
\Procedure{cg\_Server}{$\textit{filename, portnumber}$}
\State $\textit{Creates a server socket for specific port number}$\
\EndProcedure
\Procedure{cg\_Recv}{$\textit{filename, portnumber}$}
\State $\textit{Receives input files from previuos sub-jobs}$\
\EndProcedure
\Procedure{cg\_Subcall}{$\textit{filename}$}\
\State $\textit{Dynamically calls the code of the initial sub-job}$\
\EndProcedure
\Procedure{cg\_Send}{$\textit{filename}$}
\State $\textit{Sends output files to the coming generated codes}$\
\EndProcedure
\end{algorithmic}
\end{algorithm}

\begin{algorithm}
\caption{Code Generation Pseudo Code}\label{euclid}
\begin{algorithmic}[1]
\Procedure{Code\_Generation}{$\textit{filename}$} \newline 
\Comment{G is an instance of Algorithm1}
  \If {(order == 0)}:
  \State $\Call{g.cg\_Intro}{$\textit{filename}$}$
  \State $\Call{g.cg\_Server}{$\textit{filename, 12345}$}$
  \State $\Call{g.cg\_Recv}{$\textit{filename, portnumber}$}$
  \State $\Call{g.cg\_Subcall}{$\textit{filename}$}$
  \State $\Call{g.cg\_Send}{$\textit{filename}$}$
  \EndIf
  \If {(order == 1)}:
  \State $\Call{g.cg\_Intro}{$\textit{filename}$}$
  \State $\Call{g.cg\_Server}{$\textit{filename, portnumber}$}$
  \State $\Call{g.cg\_Recv}{$\textit{filename, portnumber}$}$
  \State $\Call{g.cg\_Subcall}{$\textit{filename}$}$
  \State $\Call{g.cg\_Send}{$\textit{filename}$}$
  \EndIf
  \If {(order == 2)}:
  \State $\Call{g.cg\_Intro}{$\textit{filename}$}$
  \State $\Call{g.cg\_Server}{$\textit{filename, portnumber}$}$
  \State $\Call{g.cg\_Recv}{$\textit{filename, portnumber}$}$
  \State $\Call{g.cg\_Subcall}{$\textit{filename}$}$
  \EndIf
  \If {(order == 3)}:
  \State $\Call{g.cg\_Intro}{$\textit{filename}$}$
  \State $\Call{g.cg\_Server}{$\textit{filename, 12345}$}$
  \State $\Call{g.cg\_Recv}{$\textit{filename, portnumber}$}$
  \State $\Call{g.cg\_Subcall}{$\textit{filename}$}$
  \EndIf
\EndProcedure
\end{algorithmic}
\end{algorithm}

\begin{lstlisting}[style=Bash]
Read the specifications
Build graph system
Check if the specifications and connections are correct
if(!hasCycle $$ isConnected) // This is a Valid App
	// Code generation step
	for each subjob:
		Wait(all inputs)
		if(all inputs received):
			Call subjob S
			Execute S
			Produce and send output
\end{lstlisting}