\section{Contribution}
Currently, there exist two techniques when dealing with scheduling in systems engineering. The first technique is dealing with worst case analysis in order not to violate time constraints. The other technique is to consider the average case analysis which allows us to reach optimization \cite{jaber2008using}.


In this section, we will propose a solution that allows us to run a set of applications with the best possible quality without exceeding the predefined deadline. The solution allows us to reach optimality and safety in the produced schedules. The paper uses and extends results presented in \cite{combaz2005qos} in several directions. It proposes a new control management policy that ensures better quality smoothness. In our solution, we assumed that the worst case and average case running time are known by the Quality Manager.

\begin{definition}[Application]
An application $A$ is defined by:
\begin{itemize}
\item $wc: \{1, 2, \ldots, |Q_A|\} \rightarrow \mathbb{R}^+$
\item $av: \{1, 2, \ldots, |Q_A|\} \rightarrow \mathbb{R}^+$  
\end{itemize}
\end{definition}

\begin{definition}[System]
A system $S$ is defined by a sequence of applications $A_1 \cdots A_n$
\end{definition}

\subsection{User Input}
In our model, the user should specify the following input:
\begin{itemize}
\item The applications that the user wants to run and its ordered sequence. i.e. A3, A2, A1, A1, A2 $\dot{...}$ 
\item The total time (deadline).
\item The policy to be used.
\end{itemize}

\subsection{Built-in Applications}
Our system contains the following three built-in applications which we used for the sake of testing:
\begin{itemize}
\item K-means clustering 
\item Logistic Regression 
\item Support Vector Machine
\end{itemize}
Each algorithm can be run in 5 different qualities: Q1, Q2, Q3, Q4 and Q5, where Q5 represents the best quality with the maximum number of iterations. Each quality represents a specific number of iterations.\\ \\ 
Considering the k-means clustering application:
\begin{itemize}
\item \textbf{Q1} corresponds to 1 iteration.
\item \textbf{Q2} corresponds to 3 iterations.
\item \textbf{Q3} corresponds to 5 iterations.
\item \textbf{Q4} corresponds to 6 iterations.
\item \textbf{Q5} corresponds to 10 iterations.
\end{itemize}
Considering the Logistic Regression application:
\begin{itemize}
\item \textbf{Q1} corresponds to 1 iteration.
\item \textbf{Q2} corresponds to 5 iterations.
\item \textbf{Q3} corresponds to 10 iterations.
\item \textbf{Q4} corresponds to 50 iterations.
\item \textbf{Q5} corresponds to 100 iterations.
\end{itemize}
Considering SVM application:
\begin{itemize}
\item \textbf{Q1} corresponds to 1 iteration.
\item \textbf{Q2} corresponds to 10 iterations.
\item \textbf{Q3} corresponds to 30 iterations.
\item \textbf{Q4} corresponds to 50 iterations.
\item \textbf{Q5} corresponds to 100 iterations.
\end{itemize}
\subsection{Proposed Policies}
Our system contains 4 predefined policies where each policy is suitable for a specific set of scenarios. The choice of policies for safety, quality smoothness, and optimality is analyzed in the upcoming sections.
The following are the predefined policies:
\begin{itemize}
\item \textbf{Safe Policy}: maximization of the utilization of available time budget (considers worst case running time only).
\item \textbf{Simple Policy}: simple smoothness of quality levels (considers worst case running time only).
\item \textbf{Mixed Policy}: strong smoothness of quality levels (considers both average case and worst case running time).
\item \textbf{Flexible Policy}: optimization with loose time constraints (considers average case running time only).
\end{itemize}

\FloatBarrier
\begin{figure}[H]
\centering
\includegraphics[scale=0.35]{fig/fig1.png}
\caption{\label{fig:Quality Manager}Quality Manager}
\end{figure}

\subsubsection{Safe Policy}
This policy considers only the worst case running time in order not to violate the time constraints. The algorithm will assign the first application $A_0$ with the best possible quality and accordingly will choose the minimum qualities for the rest of the applications without exceeding the total time which is assigned by the user. After an application finishes, the controller will adjust the rest of the applications and increase their quality if time permits.

\subsubsection{Simple Policy}
This policy considers only the worst case running time in order not to violate the time constraints. Initially, all applications will be assigned with the same quality. Then, the controller will adjust the quality of the rest of the applications according to the remaining time.

\subsubsection{Mixed Policy}
This policy considers both the worst and average case running time in order to reach optimality and maintain smoothness among the applications.\\ 
We define \textit{d} the difference between the execution time of the worst case scenario $C^{wc}(a_i ... a_k,q)$ and the average case scenario $C^{av}(a_i ... a_k,q)$ where $a$ represents the application and $q$ represents the quality. In other word \textit{d} = $C^{wc}(a_i ... a_k,q)$ - $C^{av}(a_i ... a_k,q)$. The mixed execution time is defined as follows $C^{mx}(a_i ... a_k,q)$ = $C^{av}(a_i ... a_k,q)$ + $d^{max}(a_i ... a_k,q)$. $d^{max}(a_i ... a_k,q)$ represents the maximum difference between the execution time of the worst case scenario $C^{wc}(a_i ... a_k,q)$ and the average case scenario $C^{av}(a_i ... a_k,q)$. In other words,  $d^{max}(a_i ... a_k,q)$= $max_{i \leq j \leq k} d(a_i ... a_k,q)$. We need to find the maximum quality $q$ such that $min(D$ - $C^{mx}(a_i ... a_k,q))$ $\geq t_{i-1}$ where $D$ represents the deadline specified by the user and $t_{i-1}$ represents the elapsed time of the previous applications.

\subsubsection{Flexible Policy}
For optimization, it considers only the average case running time. When choosing the flexible Policy, the system will consider the average running time. Using this policy, the total time used may exceed the time defined by the user, but the quality for all applications will be optimized.
