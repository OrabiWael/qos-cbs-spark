\documentclass[compress,xcolor=dvipsnames,10pt]{beamer}
\input{theme}
\input{packages}
\input{macros}
\input{tikzstyle}

\setbeamercovered{invisible}


\begin{document}
%%%%%%%%%%%%%%
\graphicspath{{figs/}}
\input{title}
%%%%%%%%%%%%%%
\begin{frame}{Outline}
\vfill
\tableofcontents[sectionstyle=show,subsectionstyle=hide]
\vfill
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

\section{Motivation}

\begin{frame}
\frametitle{Motivation: Big Data}
\begin{center}
We live in the \alert{data} age
\end{center}

\begin{columns}
\begin{column}{0.45\textwidth}
\begin{center}
\includegraphics[scale=0.2]{big-data2}
\end{center}
\end{column}
\begin{column}{0.45\textwidth}
\begin{center}
\includegraphics[scale=0.3]{socialmedia}
\end{center}
\end{column}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Hadoop MapReduce}

\begin{block}{Pros. of Hadoop MapReduce}
\begin{itemize}
\item Simple \alert{programming} model
\item Simple \alert{recovery} model 
\item Data \alert{locality}
\end{itemize}
\end{block}

~

\begin{block}{Cons. of Hadoop MapReduce}<2->
\begin{itemize}
\item Not efficient to use the same data multiple times (iterative and interactive)
\begin{itemize}
\item Intermediate results written into stable storage
\item Output of reducers written on HDFS 
\item Disk I/O, network I/O, [de]serialization 
\end{itemize}
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\frametitle{Spark}

\begin{itemize}
\item \alert{Spark} extends MapReduce model to better support two common classes analytics applications
\begin{itemize}
\item \alert{Iterative} algorithms (e.g., machine learning, graph)
\item \alert{Interactive}: efficiently analyze data sets interactively
\item Enhance \alert{programmability}:
\begin{itemize}
\item Integrated with other programming language (Scala, Java, R, Python)
\item Allow interactive mode use from Scala interpreter (\texttt{spark-shell}) and Python
\end{itemize}
\end{itemize}

~

\item<2-> Spark implements a distributed data parallel model called
\textbf{Resilient Distributed Datasets} (\alert{RDD}s)

~

\item<3-> Spark provides high-level APIs in \alert{Java}, \textcolor{blue}{Scala}, \textcolor{dkgreen}{Python} and \textcolor{purple}{R}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Motivation}
\begin{itemize}
\item \alert{Composing} and scheduling of multiple Spark applications

\vfill 

\item Real-Time \alert{Adaptive Quality} of Service of a pipeline of Spark applications
\end{itemize}
\end{frame}

\section{Reconfigurable Component-based Spark}
\begin{frame}
\frametitle{Composing of Spark Applications}
\begin{itemize}
\item Given a set of dependent Spark applications, we
define a method to \alert{automatically compose} them


\item Each spark application takes a set of \alert{inputs} (e.g., files)
and produces a set of \alert{outputs}

\item An input can be either
\alert{free} or \alert{direct}
\begin{itemize}
\item  A free input should be mapped to an
output of a different spark application
\item A direct input is mapped to a direct path
\end{itemize}
\end{itemize}

\begin{center}
\input{tikz/fig1}
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{DSL Implementation and Code Generation}
\begin{columns}
\begin{column}{0.45\textwidth}
\vfill
\begin{lstlisting}[language=java,basicstyle=\tiny\ttfamily]
{"spark-applications":[
   {"id":"SA1", "path":"path1","ni":"1","no":"2"},
   {"id":"SA2", "path":"path2","ni":"1","no":"1"},
   {"id":"SA3", "path":"path3","ni":"1","no":"1"},
   {"id":"SA4", "path":"path4","ni":"2","no":"1"},
   {"id":"SA5", "path":"path5","ni":"1","no":"1"},
]}

{"configuration":[
   { "id":"SA1", 
     "i":["pathin1"],
     "o":["patho1", "patho2"],
   },
   { "id":"SA2", 
     "i":["#SA1.o1"],
     "o":["patho1"],
   },
   { "id":"SA3", 
     "i":["#SA1.o2"],
     "o":["patho1"],
   },
   { "id":"SA4", 
     "i":["#SA2.o1", "#SA3.o1"],
     "o":["patho1"],
   },
   { "id":"SA5", 
     "i":["#SA4.o1"],
     "o":["patho1"],
   }
]}
\end{lstlisting}
\vfill
\end{column}
\begin{column}{0.52\textwidth}
{\small
\begin{enumerate}
\item \alert{\textbf{Fill input}}: inputs are mapped to their corresponding paths:
(1) \textbf{free input}: an output of different Spark application is filled; (2) \textbf{direct input}: a direct path is filled
\item \alert{\textbf{Wait signals}}: Spark applications with free inputs $freeIns$ wait for signals from other Spark applications with outputs mapped to $freeIns$
\item \alert{\textbf{Fill outputs}}: outputs are mapped to their corresponding paths
\item \alert{\textbf{Run}}: Run Spark application
\item \alert{\textbf{Send signals}}: Upon completion, Spark applications, with outputs mapped to free inputs $freeIns$, send signals to the Spark applications corresponding to $freeIns$
\end{enumerate}
}
\end{column}
\end{columns}
\end{frame}

\section{Real-Time Adaptive QoS of Spark Applications}

\begin{frame}
\frametitle{QoS of Spark Application}
\begin{itemize}
\item It is difficult to \alert{predict} the execution time of a set of Spark applications and can vary between two different runs

~

\item Deploying a set of Spark applications on the cloud can be very \alert{expensive} and can vary on-demand, i.e., depending on the needed time


~

\item<2-> The goal is to allow users to submit a set of Spark applications with a \alert{deadline} to execute all of them with the best quality!

~

\item<3-> We consider \alert{quality-based} Spark applications, i.e., each Spark application can be executed using different quality levels


~

\item<4-> We build a controller that \alert{adaptively} selects the \alert{maximum quality} levels for each Spark application while respecting the given deadline
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Parameterized Spark System}
\begin{definition}[Parameterized Spark System]
A parameterized Spark system $PSS$ is a tuple $PSS = (SA, av, wc, D)$, where 

~

\begin{itemize}
\item $SA$ is a sequence of Spark applications $SA = [SA_1, \ldots, SA_n]$

~

\item $t^{av} : SA \times Q \rightarrow \mathbb{R}^+$, average execution times

~

\item $t^{wc} : SA \times Q \rightarrow \mathbb{R}^+$, worst-case execution times

~

\item $D \in \mathbb{R}^+$ is the global deadline to be met
\end{itemize} 
\end{definition}
\end{frame}

\begin{frame}
\frametitle{Correct Over-Approximation of Execution Times}
\begin{itemize}
\item Let $t_{i-1}$ be the actual execution time to execute $SA_1, \ldots, SA_{i-1}$

~

\item That is, the time to execute the remaining Spark applications (i.e., $SA_i,\ldots,SA_n$) is $D - t_{i-1}$

~
 
\item The controller must select the maximum quality levels for the remaining Spark applications while respecting the new deadline

~

\item Let $t^x_{e}$ to be an estimation (over-approximation) of the execution time of a sequence of Spark applications for a given quality, $t^x_e([SA_i,\ldots,SA_n], q)$
\end{itemize}

\begin{center}
\alert{How to compute $t^x_{e}$?}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Policies to Estimate Execution Times: Safe Policy}
We distinguish several \alert{policies} to \alert{estimate} execution times of the remaining applications:

~

\begin{itemize}
\item \alert{Safe} Policy

~

\item \alert{Simple} Policy

~

\item \alert{Mixed} Policy
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Safe Policy}

\begin{block}{Safe Policy ($t^{sf}_{e}$)}  
\begin{itemize}
\item This policy defines an obvious policy to ensure \alert{safety} (deadline is met)
\item But it gives \alert{more priority/time} (i.e., higher quality) to first applications
\end{itemize}

$$t^{sf}_{e}([SA_i, \ldots, SA_n], q) = t^{wc}(SA_i, q) + \sum_{k=i+1}^{n}t^{wc}(SA_k, q_{min})$$
\end{block}

\end{frame}

\begin{frame}
\frametitle{Simple Policy}

\begin{block}{Simple Policy ($t^{sp}_{e}$)}  
\begin{itemize}
\item This introduce another policy to ensure \alert{safety} 
\item It also improves \alert{smoothness} (distributed quality over all the remaining applications) by combining worst-case and average case execution times
\end{itemize}


$$t^{sp}_{e}([SA_i, \ldots, SA_n], q) = \mathtt{Max} \, \big\{ t^{sf}_{e}([SA_i, \ldots, SA_n], q),\, t^{av}_{e}([SA_i, \ldots, SA_n], q)\big\}$$
where $t^{av}_{e}([SA_i, \ldots, SA_n], q) = \sum_{k=i}^{k=n} t^{av}(SA_i, q)$
\end{block}

\end{frame}

\begin{frame}
\frametitle{Mixed Policy}
\begin{block}{Mixed Policy ($t^{mx}_{e}$)}
The mixed policy defines a \alert{generalization} of the simple policy to improve \alert{smoothness} by taking into account all possible sequences

$$t^{mx}_{e}([SA_i, \ldots, SA_n], q) = \mathtt{Max}_{k=i-1}^{k=n} \, \{t^{av}_{e}([SA_i, \ldots, SA_k], q) \,+\, t^{sf}_{e}([SA_{k+1}, \ldots, SA_n], q)\}$$
\end{block}
\end{frame}




\begin{frame}
\frametitle{Controller: Adaptive QoS}

\begin{center}
The controller iteratively selects maximum quality levels (\alert{optimality}) while guaranteeing \alert{safety} (deadline must be met)

~

\scalebox{0.75}{
\input{tikz/qos.tex}
}
\end{center}

~

\alert{At each iteration} $i$, the controller selects (depending on the policy used) the quality level of the next Spark application according to the following: 
$$
\mathtt{Max} \, \{q \, \mid \, t^{x}_{e}([SA_i, \ldots, SA_n], q) + t_{i-1} \leq D\}
$$
%Where $t_{i-1}$ is the actual execution time after executing $SA_1, \ldots, SA_{i-1}$, $SA_i$ is the current application to be executed and $t^{x}_{e}$ is an estimation of the execution time to execute the reaming applications, i.e., $SA_i, \ldots, SA_n$. This estimation can be computed using one of the three policies defined in Subsection~\ref{sec:policies} (i.e., average, simple, mixed). 
\end{frame}

\section{Experimental Results}

\begin{frame}
\frametitle{Experimental Results: Real Time Adaptive QoS}
\begin{itemize}
\item We consider three \alert{built-in} Spark applications using MLlib (1) K-means clustering; (2) Logistic Regression; and (3) Support Vector Machine (SVM)

~

\item We adapt these application to run in \alert{five different quality} levels from $1$ to $5$ (highest quality)

~

\item Each quality represents a specific \alert{number of iterations} (higher quality more iterations)

~

\item For example, quality level $4$ and $5$ in case of SVM correspond to $50$ and $100$ iterations, respectively
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Worst and Average Case Execution Times}

\begin{table}
\centering
\begin{tabular}{|l|c|c|c|c|c|}
\hline
\textbf{Algorithms} & \textbf{Q1}  &  \textbf{Q2} &  \textbf{Q3} & \textbf{Q4} & \textbf{Q5}\\ \hline
K-means                & 115s  & 125s  & 135s & 140s & 145s    \\  \hline
Logistic Regression    & 120s  & 135s  & 140s & 150s & 155s  \\ \hline
SVM & 130s  & 170s  & 190s & 235s  & 345s  \\ \hline
\end{tabular}
\caption{Worst case running time.}
\end{table}

~

\begin{table}
\centering
\begin{tabular}{|l|c|c|c|c|c|}
\hline
\textbf{Algorithms} & \textbf{Q1}  &  \textbf{Q2} &  \textbf{Q3} & \textbf{Q4} & \textbf{Q5}\\ \hline
K-means & 95s  & 115s   & 125s  & 130s  & 135s   \\ \hline
Logistic Regression    & 110  & 125s   & 130s  & 145s  & 150s   \\ \hline
SVM & 90s   & 140s   & 160s  & 205s  & 315s  \\ \hline
\end{tabular}
\caption{Average case running time.}
\end{table}

\end{frame}

\begin{frame}
\frametitle{Quality Levels}
Selected Quality by the Controller using Safe, Simple and Mixed Policies
\begin{center}
\includegraphics[scale=0.45]{bench/bench-all-crop.pdf}
\end{center}
\end{frame}

\section{Conclusion and Future Work}
\begin{frame}
\frametitle{Conclusion and Future Work}
{\small
\begin{block}{Conclusion}
\begin{itemize}
\item We have presented a new approach for \alert{linking} Spark applications to build a complex one based on a user-defined configuration file
\item We have implemented a \alert{Domain Specific Language} (DSL) to easily define interfaces as well as connections between them
\item \alert{Automatic} build the final system with respect to the input configuration
\item Augment clusters with controllers in order to \alert{automatically manage quality levels} of a sequence of Spark applications
\end{itemize}
\end{block}
}

{\small
\begin{block}{Future Work}<2->
\begin{itemize}
\item DSL that combines \alert{several} frameworks and not only Spark
\item Introduce interface \alert{place holders} within a Spark application to allow more expressive composition
\item Support \alert{several clusters} and hence efficiently transfer data at synchronization points become a challenging task. 
\item Extend the controller to take as input a \alert{graph} of Spark applications
\end{itemize}
\end{block}
}
\end{frame}


%\input{motivation}
%\input{bip}
%\input{re}
%\input{re-bip}
%\input{experiments}
%\input{conclusion}

\end{document}
