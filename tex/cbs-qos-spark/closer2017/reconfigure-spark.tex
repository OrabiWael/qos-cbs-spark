\section{Reconfiguration of Spark Applications}
\label{sec:cbs-spark}
Given a set of dependent Spark applications, we define a method to automatically compose them. 
Each spark application takes a set of inputs (e.g., files) and produces a set of outputs. 
An input can be either free or direct. A free input should be mapped to an output of a different spark application. A direct input is mapped to a direct path.

Formally, a spark application is defined as follows: 

\begin{definition}[Spark Application]
A spark application $SA$ is defined as a set of tuple $(ins, outs)$, where:
\begin{itemize}
\item $ins = freeIns \cup directIns$ is the set of inputs;
\item $outs$ is the set of outputs.
\end{itemize}
\end{definition}

Given a user-specified configuration, spark applications are composed by mapping free inputs of spark applications to outputs of other applications. Formally a configuration is defined as follows:
\begin{definition}[Configuration]
Given a set of spark application $\{SA_i\}_{i\in I}$, a configuration $C$ is a function defined by $C: FreeInput \rightarrow Output$, where:
\begin{itemize}
\item $FreeInput = \bigcup_{i \in I}SA_i.freeIns$;
\item $Output = \bigcup_{i \in I}SA_i.outs$.
\end{itemize} 
\end{definition}

\begin{figure}
\centering
\scalebox{0.9}{\input{tikz/fig1}}
\caption{Representation of dependencies between sub-jobs}
\label{example:spark-cbs}
\end{figure} 


\begin{example}
Figure \ref{example:spark-cbs} shows an example of composing spark applications. The system consists of five Spark applications $SA_1, \ldots, SA_5$. $SA_1$ has one direct input $i_1$ and two outputs $o_1$ and $o_2$. $SA_2$ has one free input $i_1$ and one output $o_1$. First output $o_1$ of $SA_1$ is mapped to the free input $i_1$ of $SA_2$.
\end{example}


Given a set of spark applications $\{SA_i\}_{i\in I}$, a configuration $C$ must be valid. A configuration is valid iff:
\begin{itemize}
\item Free inputs are mapped to outputs of different applications. That is, if $C(SA_i.fi_1) = SA_j.o_1$, then $i \neq j$, where $fi_1$ is a free input in $SA_i$ and $o_1$ is an output in $SA_j$.
\item The graph directed $G$ obtained from $\{SA_i\}_{i\in I}$ and $C$ does not contain cycles, where the set of vertices of $G$ represents Spark applications and the set of edges represents the mapping between them. Formally, $G = (\{SA_i\}_{i\in I}, E)$, where $E = \{(SA_i, SA_j) \mid \exists freeIn \in SA_i.freeIns \wedge out \in SA_j.outs:  C(freeIn) = out \}$.
\end{itemize}

\begin{example}
The system defined in Figure \ref{example:spark-cbs} is valid since (1) all free inputs are mapped to outputs of different applications; and (2) the graph obtained by connecting outputs to free inputs is acyclic. 
\end{example}


\subsection{Semantics and Code Generation}
Given a set of Spark applications and a configuration $C$, we first check the validity of  configuration $C$, then we automatically synthesize the glue of each Spark application that corresponds to configuration $C$. A glue surrounds each Spark application by the corresponding synchronization and communication primitives. The code generation is depicted in Listing~\ref{code:code-generation-cbs-spark}. It mainly consists of the following five steps: 
\begin{lstlisting}[language=java,caption={Automatic Glue Generation of a Spark Application},label={code:code-generation-cbs-spark}]
/*@\textcolor{dkgreen}{\textbf{Step 1: Fill Inputs}}@*/
for(input $\in$ SAi.ins) {
   if( isFreeInput(input) ) {
      source = input.from;
      index = input.index;
      inputs.add(source.getInput(index));
   } else {
      inputs.add(input.value);   
   }
}

/*@\textcolor{dkgreen}{\textbf{Step 2: Wait Signals}}@*/
for(input $\in$ SAi.freeIns) {
   waitSignal(input.from);
}

/*@\textcolor{dkgreen}{\textbf{Step 3: Fill Outputs}}@*/
for(output $\in$ SAi.outs) {
   outputs.add(output);
}

/*@\textcolor{dkgreen}{\textbf{Step 4: Run Spark Application}}@*/
run("path", inputs, outputs);

/*@\textcolor{dkgreen}{\textbf{Step 5: Send Signals}}@*/
for(output $\in$ SAi.outs) {
   free-inputs = $C^{-1}$(output)
   for(free-input $\in$ free-inputs) {
      sendSignal(free-input.id);   
   }
}
\end{lstlisting}
\begin{enumerate}
\item Fill input: inputs are mapped to their corresponding paths. In case of a free input, an output of different Spark application is filled. In case of direct input a direct path is filled. 
\item Wait signals: Spark applications with free inputs $freeIns$ wait for signals from other Spark applications with outputs mapped to $freeIns$. This phase may require a setup phase to set the connections between processes (e.g., sockets, shared semaphores, signals).
\item Fill outputs: outputs are mapped to their corresponding paths.
\item Run: Run Spark application.
\item Send signals: Upon completion, Spark applications, with outputs mapped to free inputs $freeIns$, send signals to the Spark applications corresponding to $freeIns$.
\end{enumerate}



\subsection{DSL Implementation}
We define a Domain Specific Language (DSL) using JSON representation that defines Spark applications and a configuration to connect them. 
Each Spark application is identified by an identifier $id$, a path where the Spark program exists $path$, number of inputs $ni$, and number of outputs $no$. 
Then, the configuration maps inputs to direct locations or to output of other Spark applications (in case of free inputs).
Listing~\ref{code:config-file-cbs-spark} depicts the general structure to specify a set of Spark applications and a configuration. It mainly consists of two parts: 
\begin{itemize}
\item The first part defines the set of Spark applications with their corresponding interfaces (i.e., an identifier, location, number of inputs and number of outputs). 
\item The second part defines the configuration which connects Spark applications (i.e., connect output to free inputs).
\end{itemize} 


\begin{lstlisting}[language=xml,caption = {General shape of a configuration file.},label={code:config-file-cbs-spark}]
{"spark-applications":[
   {"id":"id", "path":"path","ni":"n","no":"n"},
   {"id":"id", "path":"path","ni":"n","no":"n"},
  ...
   {"id":"id", "path":"path","ni":"n","no":"n"},
]}

{"configuration":[
   { "id":"id", 
     "i":["i1", "i2", ...],
     "o":["o1", "o2", ...],
   },
   { "id":"id", 
     "i":["i1", "i2", ...],
     "o":["o1", "o2", ...],
   },
   ...
   { "id":"id", 
     "i":["i1", "i2", ...],
     "o":["o1", "o2", ...],
   }
]}
\end{lstlisting}

Listing~\ref{code:example-cbs-spark} shows an example of system that corresponds to Figure~\ref{example:spark-cbs}. It consists of two parts: (1) spark applications, and (2) configuration. 
Spark application $SA_1$ has one input, which is a direct input, and two outputs. Spark application $SA_2$ has one input and one output. The first input is mapped to the output of Spark application $SA_1$ (i.e., $\mathtt{\#SA1.o1}$). 
 
\begin{lstlisting}[language=xml,caption = {Configuration File Corresponding to Figure~\ref{example:spark-cbs}},label={code:example-cbs-spark}]
{"spark-applications":[
   {"id":"SA1", "path":"path1","ni":"1","no":"2"},
   {"id":"SA2", "path":"path2","ni":"1","no":"1"},
   {"id":"SA3", "path":"path3","ni":"1","no":"1"},
   {"id":"SA4", "path":"path4","ni":"2","no":"1"},
   {"id":"SA5", "path":"path5","ni":"1","no":"1"},
]}

{"configuration":[
   { "id":"SA1", 
     "i":["pathin1"],
     "o":["patho1", "patho2"],
   },
   { "id":"SA2", 
     "i":["#SA1.o1"],
     "o":["patho1"],
   },
   { "id":"SA3", 
     "i":["#SA1.o2"],
     "o":["patho1"],
   },
   { "id":"SA4", 
     "i":["#SA2.o1", "#SA3.o1"],
     "o":["patho1"],
   },
   { "id":"SA5", 
     "i":["#SA4.o1"],
     "o":["patho1"],
   }
]}
\end{lstlisting}