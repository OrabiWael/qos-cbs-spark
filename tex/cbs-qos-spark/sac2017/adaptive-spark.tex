\section{Real-Time Adaptive QoS of Spark Application}
\label{sec:adaptive-spark}
Quality of Service (QoS) applications such as machine learning and graph analytics, iteratively process the data. That is multiple rounds of computation are performed on the same data. 
Therefore, the number of rounds can be easily tuned to achieve a better quality of the end result. In general, the quality is proportional to the number of rounds, and the execution times of applications are unknown increasing functions with respect to quality level parameters (i.e., increase the quality implies increase of execution time). 
In case of big data, these applications can be easily and efficiently implemented using Spark which provides: (1) scalability; (2) implicit distribution; (3) fault tolerance.
 
In this section, we propose an adaptive method for QoS management of sequence of Spark applications. Given a sequence of parameterized spark applications and a deadline to be met, the method allows adapting the behavior of each application by adequately setting its quality level parameter while respecting the given deadline. The objective is to not miss the deadline and to select the maximum quality levels.

We consider Spark applications for which it is possible, by using timing analysis and profiling techniques, to compute estimate of worst-case execution times and average execution times for different quality levels. Worst case and average execution times will be used to estimate the execution times of Spark applications, and hence select the best quality while not exceeding the deadline. 

We implement a controller that iteratively takes as input the current Spark application $SA_i$ to be executed and the actual time\footnote{Note that the actual execution times will be only known after executing the application and will be used to adapt the remaining deadline.} of the previously executed Spark applications, and it selects the quality level $q$ to execute $SA_i$ so that we would be able to execute the remaining applications  while not exceeding the deadline. Then, the new deadline will be computed with respect to the actual execution time of $SA_i(q)$. The controller must maximize the overall quality levels. For this, it may follow several strategies. For instance, it may select the maximum quality level of the current application, however, this will end up by giving priority for first applications (i.e., selecting maximum quality) and low priority last applications (i.e., selecting minimum quality). 
Another option is to make the selected quality levels smooth (i.e., low deviation of quality levels with respect to the average quality). 
Consequently, we implement three different policies that are supported by the controller: (1) safe; (2) simple; (3) mixed. 

\subsection{Controller - Proposed Policies}
\label{sec:policies}
We consider parameterized Spark applications, i.e., their execution times depend on the input quality. Moreover, non-predictability of the underlying cluster is another factor of uncertainty. For this, we consider that the execution times are unknown but bounded. 
A parameterized application can be defined as follows.  

\begin{definition}[Parameterized Spark System]
A parameterized Spark system $PSS$ is a tuple $PSS = (SA, av, wc, D)$, where 
\begin{itemize}
\item $SA$ is a sequence of Spark applications $SA = [SA_1, \ldots, SA_n]$.
\item $t^{av} : SA \times Q \rightarrow \mathbb{R}^+$, is a function that returns the average execution time, $t^{av}(SA_i, q)$ for a given Spark application $SA_i$ and a quality level $q$. $Q = [q_{min}; q_{max}]$ is a finite interval of integers denoting quality levels. We assume that, for all $SA_i \in SA$, $q \mapsto t^{av}(SA_i, q)$ is a non-decreasing function.
\item $t^{wc} : SA \times Q \rightarrow \mathbb{R}^+$, is a function that returns the worst-case execution time, $t^{wc}(SA_i, q)$ for a given Spark application $SA_i$ and a quality level $q$. We assume that, for all $SA_i \in SA$, $q \mapsto t^{wc}(SA_i, q)$ is a non-decreasing function.
\item $D \in \mathbb{R}^+$ is the global deadline to be met. 
\end{itemize} 
\end{definition}

Let $t_{i-1}$ be the actual execution time to execute $SA_1, \ldots, SA_{i-1}$. That is, the time to execute the remaining Spark applications (i.e., $SA_i,\ldots,SA_n$) is $D - t_{i-1}$. 
The controller must select the maximum quality levels for the remaining Spark applications while respecting the new deadline. We define $t^x_{e}$ to be an estimation of the execution time of a sequence of Spark applications for a given quality, $t^x_e([SA_i,\ldots,SA_n], q)$.  
Since our goal is not to exceed the deadline, our estimation should be an over-approximation (i.e., based on worst-case execution times) of the actual execution time. 
We distinguish several policies to estimate execution times of the remaining applications. 

\textit{\textbf{Safe Policy ($t^{sf}_{e}$):}}  this defines an obvious policy to ensure safety (deadline is met) but gives more priority/time (i.e., higher quality) to first applications. Given a sequence of Spark applications and a quality level $q$, $t^{sf}_{e}$ is defined as follows: $t^{sf}_{e}([SA_i, \ldots, SA_n], q) = t^{wc}(SA_i, q) + \sum_{k=i+1}^{n}t^{wc}(SA_k, q_{min})$.

\textbf{\textbf{Simple Policy ($t^{sp}_{e}$):}}  this defines another policy to ensure safety and improves smoothness (distributed quality over all the remaining applications) by combining worst-case and average case execution times. $t^{sp}_{e}$ is defined as follows: $t^{sp}_{e}([SA_i, \ldots, SA_n], q) = \mathtt{Max} \, \big\{ t^{sf}_{e}([SA_i, \ldots, SA_n], q),\, t^{av}_{e}([SA_i, \ldots, SA_n], q)\big\}$, where $t^{av}_{e}([SA_i, \ldots, SA_n], q) = \sum_{k=i}^{k=n} t^{av}(SA_i, q)$. 

\textit{\textbf{Mixed Policy ($t^{mx}_{e}$):}}  the mixed policy defines a generalization of the simple policy to improve smoothness by taking into account all possible sequences. 
$t^{mx}_{e}$ is defined as follows: 
$t^{mx}_{e}([SA_i, \ldots, SA_n], q) = \mathtt{Max}_{k=i-1}^{k=n} \, \{t^{av}_{e}([SA_i, \ldots, SA_k], q) + t^{sf}_{e}([SA_{k+1}, \ldots, SA_n], q)\}$.

For a given quality $q$, the mixed policy has a higher over-estimation of the execution time than the simple policy. Moreover, the latter has a higher over-estimation of the execution time than the safe policy. Formally, the above policies satisfy the following property:
$t^{mx}_{e}([SA_i, \ldots, SA_n], q) \geq t^{sp}_{e}([SA_i, \ldots, SA_n], q) \geq t^{sf}_{e}([SA_i, \ldots, SA_n], q)$. 

Moreover, as the safe policy is based on worst-case execution times, it clearly satisfies the safety requirement (i.e., deadline not exceeded at all iterations). Consequently, all the policies satisfy the safety requirement. 

\subsection{Controller - Quality Manager Algorithm}
The controller must iteratively select maximum quality levels (optimality) while guaranteeing safety (deadline must be met). For this, at each iteration, it selects (depending on the policy used) the quality level of the next Spark application according to the following: 

$$
\mathtt{Max} \, \{q \, \mid \, t^{x}_{e}([SA_i, \ldots, SA_n], q) + t_{i-1} \leq D\}
$$

Where $t_{i-1}$ is the actual execution time after executing $SA_1, \ldots, SA_{i-1}$, $SA_i$ is the current application to be executed and $t^{x}_{e}$ is an estimation of the execution time to execute the reaming applications, i.e., $SA_i, \ldots, SA_n$. This estimation can be computed using one of the three policies defined in Subsection~\ref{sec:policies} (i.e., average, simple, mixed). 

\begin{example}
Given a sequence of three Spark applications $SA_1, SA_2, SA_3$ with deadline equals to $9$, where the average (avet), worst case (wcet) and the actual (acet) execution times are given in the following table.  
 
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
quality & wcet & avet & acet\\ \hline
1 & 1 & 1 & 1 \\ \hline
2 & 5 & 2 & 2 \\ \hline
3 & 6 & 3 & 3 \\ \hline
4 & 7 & 4 & 4 \\ \hline
\end{tabular}
\end{center}
\begin{itemize}
\item In case of safe policy, the quality levels $4, 1, 1$ will be selected for applications $SA_1, SA_2, SA_3$, respectively. 
\item In case of simple policy, the quality levels $3, 2, 1$ will be selected for applications $SA_1, SA_2, SA_3$, respectively. 
\item In case of mixed policy, the quality levels $2, 2, 2$ will be selected for applications $SA_1, SA_2, SA_3$, respectively. 
\end{itemize}
\end{example}

\subsection{Experimental Results}
We consider three built-in Spark applications using MLlib~\cite{mllib1,mllib2} (1) K-means clustering; (2) Logistic Regression; and (3) Support Vector Machine (SVM). We adapt these application to run in five different quality levels from $1$ to $5$ (highest quality). Each quality represents a specific number of iterations (higher quality more iterations). For example, quality level $4$ and $5$ in case of SVM correspond to $50$ and $100$ iterations, respectively.  

We compute the average and worst-case execution times by running several benchmarks. 
Tables \ref{tab:wcet} and \ref{tab:avet} depicts the average and worst-case execution times for specific number of instances and features.  Note that these execution times depend on the parameters of each algorithm (e.g., number of features, size of training set). For this, in the general case, the average and worst-case execution times are functions with respect to input parameters. For the sake of simplicity, we only consider fixed value of parameters. Moreover,  we consider that the deadline is greater than the summation of worst case execution times of the lowest quality. 
\begin{table}[H]
\centering
\begin{tabular}{|l|c|c|c|c|c|}
\hline
\textbf{Algorithms} & \textbf{Q1}  &  \textbf{Q2} &  \textbf{Q3} & \textbf{Q4} & \textbf{Q5}\\ \hline
K-means                & 115s  & 125s  & 135s & 140s & 145s    \\  \hline
Logistic Regression    & 120s  & 135s  & 140s & 150s & 155s  \\ \hline
SVM & 130s  & 170s  & 190s & 235s  & 345s  \\ \hline
\end{tabular}
\caption{Worst case running time.}
\label{tab:wcet}
\end{table}

\begin{table}[H]
\centering
\begin{tabular}{|l|c|c|c|c|c|}
\hline
\textbf{Algorithms} & \textbf{Q1}  &  \textbf{Q2} &  \textbf{Q3} & \textbf{Q4} & \textbf{Q5}\\ \hline
K-means & 95s  & 115s   & 125s  & 130s  & 135s   \\ \hline
Logistic Regression    & 110  & 125s   & 130s  & 145s  & 150s   \\ \hline
SVM & 90s   & 140s   & 160s  & 205s  & 315s  \\ \hline
\end{tabular}
\caption{Average case running time.}
\label{tab:avet}
\end{table}


%\begin{figure}[H]
%\centering
%\includegraphics[scale=0.31]{bench/bench-sf.pdf}
%\caption{Selected Quality by the Controller using Safe Policy.}
%\label{fig:bench-quality-safe}
%\end{figure}

We test our implementation by considering a sequence of seven Spark applications: 
$SA_1$ (SVM), $SA_2$ (Logistics Regression), $SA_3$ (K-means), $SA_4$ (Logistics Regression), $SA_5$ (K-means), $SA_6$ (SVM) and $SA_7$ (SVM). The deadline is $1000$ seconds. Figure~\ref{fig:bench-quality-all} depicts the quality levels selected by the controller using safe, simple and mixed policies, respectively. 
%
%\begin{figure}[H]
%\centering
%\includegraphics[scale=0.31]{bench/bench-sp.pdf}
%\caption{Selected Quality by the Controller using Simple Policy.}
%\label{fig:bench-quality-simple}
%\end{figure}
%
Clearly, the quality levels selected using the safe policy gives more priority to first applications. The simple policy provides a small smoothness of quality levels. The mixed policy provides a fair distribution (good smoothness) of quality levels between all applications. 
%
\begin{figure}[H]
\centering
\includegraphics[scale=0.35]{bench/bench-all.pdf}
\caption{Selected Quality by the Controller using Safe, Simple and Mixed Policies.}
\label{fig:bench-quality-all}
\end{figure}
%
%
%\begin{figure}[H]
%\centering
%\includegraphics[scale=0.31]{bench/bench-mx.pdf}
%\caption{Selected Quality by the Controller using Mixed Policy.}
%\label{fig:bench-quality-mixed}
%\end{figure}


