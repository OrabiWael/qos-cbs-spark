\documentclass[final,hyperref={pdfpagelabels=false}]{beamer}
\mode<presentation>
  {
  %  \usetheme{Berlin}
  \usetheme{Dreuw}
  }
  \usepackage{times}
  \usepackage{amsmath,amsthm, amssymb, latexsym}
  \boldmath
  \usepackage[english]{babel}
  \usepackage[latin1]{inputenc}
   \usepackage[orientation=portrait,size=a0,scale=1.4,debug]{beamerposter}
\input{packages}
\setbeamertemplate{caption}{\insertcaption}


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
  \graphicspath{{figures/}}
  \title{User-Based Load Balancer in HBase}
  \author{Ahmad Ghandour, Mariam Moukalled, Mohamad Jaber and Yli\`es Falcone}
  \institute[AUB and UGA]{American University of Beirut and Univ. Grenoble Alpes}


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
  \begin{document}
  
  \begin{frame}[fragile]
\vfill     
     \begin{block}{\large HBase Overview}
  \begin{columns}
  \begin{column}{0.4\textwidth}
  \begin{block}{Big Data Storage Challenge - HBase}
  \begin{itemize}
  \item The performance of \textbf{relational databases} is limited by the amount of data
    \item \textbf{Challenges}: Scalability, high performance and high availability
  \item \textbf{Solution}: Not only SQL databases (NoSQL)
 \item \textbf{HBase} is a NoSQL database and part of the Hadoop ecosystem that provides random real-time
read/write access to data in the \textbf{Hadoop} Distributed File System (HDFS)
 \item In HBase a table is a collection of \textbf{rows} sorted according to keys
\item HBase system is composed of several \textbf{components} that formulate a hierarchical structure
  \end{itemize}
  \end{block}
  \end{column}
    \begin{column}{0.37\textwidth}
     \begin{block}{Zookeeper}
  {\small Clients communicate with \textbf{region servers} via \textbf{zookeeper}. Zookeeper maintains \texttt{METADATA} and provides distributed synchronization. It also tracks server failures or network partitions}
  \end{block}
  
  ~
  
      \includegraphics[scale=2]{hbase}
  \end{column}  
  \begin{column}{0.15\textwidth}
 
  
    \begin{block}{HMaster}
   {\small Manages region servers (e.g., load balancing, schema changes)}
  \end{block}
  
    \begin{block}{Region Server}
   {\small Stores regions and handles all read/write requests for all
its regions}
  \end{block}
  
      \begin{block}{Region}
   {\small Regions are 
tables (blocks) that are split up and spread
across the region servers}
  \end{block}
  \end{column}
  \end{columns}
    \end{block}  
    \vfill
        \begin{block}{\large Problem Definition, Challenges \& Methodology}
  \begin{columns}
  \begin{column}{0.4\textwidth}
  \begin{block}{Problem Definition}
  \begin{itemize}
  \item Default \textbf{distribution} is based on average data load between servers
  \item Accesses to key/value data may not be \textbf{homogeneous}
  \item \textbf{Hot} key/value distribution 
  \begin{itemize}
  \item Can not be configured at time of data upload
\item Dynamics based on client requests
  \end{itemize}
\item DB admin \textbf{manual} intervention to apply splitting
\item Pre-configured distribution policy (e.g., \textbf{constant} size split) 
  \end{itemize}
  \end{block}
  
  ~
   
  \begin{block}{Challenges}
  \begin{itemize}
  \item Optimize \textbf{system throughput} by decreasing latency requests time
\item Better \textbf{resource allocation} among all region servers
\item Study the \textbf{monitored} data to detect future plans for data distribution among regions
  \end{itemize}
  \end{block}
  \end{column}
    \begin{column}{0.55\textwidth}
     \begin{block}{Algorithm (dynamically change region distribution based on tracing user's request)}
     \begin{enumerate}
     \item The first phase consists in \textcolor{red}{\textbf{selecting}} the top $k$ victim regions $VRs$ with average response time greater than some threshold, $ART^{\mathtt{threshold}}$
  \begin{lstlisting}[language=java,basicstyle=\footnotesize\ttfamily]
/*** Select Victim Regions ***/
ART = averageRTPerRegion();
VRs = filterVitimRegions(ART);
\end{lstlisting}
\item For every victim region $vr \in VRs$ of size greater than $RS^\mathtt{threshold}$, we apply the \textcolor{blue}{\textbf{split}} phase. We split with respect to a balance key to guarantee that after the split the load of requests is equally partitioned
    \begin{lstlisting}[language=java,basicstyle=\footnotesize\ttfamily]
/*** Split Phase ***/
for(vr: VRs) {
   if( size(vr) < RS_THRESHOLD ) continue;
   TopKKeys = getTopKKeys(vr);
   if( size(TokKKeys) == 1 ) {
      balanceKey = middleKey(vr);  
   } else {
      requestAverage = sumRequests(TokKKeys);
      betaIndex = computeBetaIndex(TopKKeys);
      balanceKey = (TopKKeys[betaIndex] + TopKKeys[betaIndex + 1]) / 2;
   }
   split(vr, balanceKey);
}
\end{lstlisting}
\item \textcolor{green}{\textbf{Move}} regions from high-loaded region servers to low-loaded regions servers
\begin{lstlisting}[language=java,basicstyle=\footnotesize\ttfamily]
/*** Move Phase ***/
for(vr: VRs) {
   RS = regionServer(vr);
   if( numberRegions(RS) <= NR_THRESHOLD ) continue;
   count = numberRegions(RS) - NR_THRESHOLD;
   RM = selectRegionsToMove();
   move(count, vr, RM);
}
\end{lstlisting}
     \end{enumerate}
\end{block}
  \end{column}  
  \end{columns}
    \end{block} 
    \vfill
    \begin{columns}
    \begin{column}{0.6\textwidth}
    \begin{block}{\large Evaluation (Cluster of 8 nodes, region size $4$GB)}   
      \begin{columns}
      \centering
      \begin{column}{0.45\textwidth}
     \begin{table}[t]
\small
\begin{tabular}{| c | c | c | c | }
\hline
Requests &	RT Before & 	RT After & Improvement \\ \hline \hline	
100,000	&26.61	&19.6	&26.34\%\\ \hline
250,000	&52.39	&48.85	&6.76\%\\ \hline
500,000	&101.97	&93.61	&8.20\%\\ \hline
750,000	&153.64	&140.10	&8.81\%\\ \hline
1,000,000	&203.43	&183.80	&9.65\%\\ \hline
\end{tabular}
\caption{1 client - 2 hot keys}
\end{table}
\end{column}

\centering
      \begin{column}{0.45\textwidth}
\begin{table}[t]
\small
\begin{tabular}{| c | c | c | c | }
\hline
Requests &	RT Before & 	RT After & Improvement \\ \hline \hline	
100,000	&40.71	&36.60	&10.10\%\\ \hline
250,000	&97.70	&88.86	&9.05\%\\ \hline
500,000	&196.82	&162.69	&17.34\%\\ \hline
750,000	&300.52	&245.01	&18.47\%\\ \hline
1,000,000	&410.46	&326.61	&20.43\%\\ \hline
\end{tabular}
\caption{1 client - 4 hot keys}
\label{tab:key-4-client-1}
\end{table}
\end{column}
\end{columns}

\begin{columns}
\centering
      \begin{column}{0.45\textwidth}
\begin{table}[b]
\small
\begin{tabular}{| c | c | c | c | }
\hline
Requests &	RT Before & 	RT After & Improvement \\ \hline \hline	
100,000	&40.45	&35.48	&12.29\%\\ \hline
250,000	&133.23	&83.15	&37.59\%\\ \hline
500,000	&178.17	&164.58	&7.63\%\\ \hline
750,000	&263.9	&244.48	&7.36\%\\ \hline
1,000,000	&365.54	&325.56	&10.94\%\\ \hline
\end{tabular}
\caption{4 clients - 2 hot keys}
\label{tab:key-2-client-4}
\end{table}
\end{column}

\centering
      \begin{column}{0.45\textwidth}
%
\begin{table}[b]
\small
\begin{tabular}{|c | c | c | c | c | }
\hline
Requests &	RT Before & 	RT After & Improvement \\ \hline \hline	
100,000	&67.07	&61.13	&8.86\%\\ \hline
250,000	&167.96	&151.37	&9.88\%\\ \hline
500,000	&329.74	&298.64	&9.43\%\\ \hline
750,000	&511.57	&448.59	&12.31\%\\ \hline
1,000,000	&658.01	&595.65	&9.48\%\\ \hline
\end{tabular}
\caption{4 clients - 4 hot keys}
\label{tab:key-4-client-4}
\end{table}
\end{column}
      \end{columns}
          \end{block}
    \end{column}
        \begin{column}{0.38\textwidth}
        \begin{block}{Conclusion}
        \begin{itemize}
        \item We proposed an algorithm that enhances client operation
latency by (1) \textbf{monitoring} and (2) \textbf{dynamically balancing} the region
servers of HBase system 
\item The algorithm is based on the \textbf{most requested}
keys and the average response time of clients' requests
\item The evaluation shows that our algorithm reduces
the \textbf{latency} of client requests in case where
some keys are highly requested
\end{itemize}
\end{block}
        \begin{block}{Future Works}
        \begin{itemize}
\item Extend our algorithm to take into account \textbf{write operations}
\item Testing our algorithm on \textbf{larger} clusters and table
\item Automatic \textbf{compaction} of regions
\end{itemize}
                \end{block}
    \end{column}
    \end{columns}
\end{frame}
   
\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% End:
