\section{Adaptive HBase}
In this section, we introduce an efficient user-based balancing method, which automatically  splits and moves HBase regions to improve response time of clients' requests. The method mainly consists of two phases.   
First, we integrate a monitoring feature to the APIs provided by HBase that allow clients to access data. Second, we implement an algorithm to automatically detect regions causing overhead, and accordingly regions are split or moved to improve the performance of the clients' request. 

\subsection{Runtime Monitoring and Profiling}
The first step is to detect regions causing overhead. For this, we intercept the HBase API to monitor the load of the region servers. The interception allows to keep track of the keys accessed per regions, the total number of accesses per key and the average response time with respect to each region. The profiling data is stored in the META table. 
Consequently, whenever a client requests to get any key using shell-based or API, the new integrated code allows to monitor and store the required information to the META table.
Note that the META table remains on one of the region server and it does not split regardless of its size. Moreover, the new column family containing the logs is cleared each time we execute the balancer, which is defined in the following.  


\subsection{Runtime User-based Load Balancing}
We implement a user-based load balancer, which dynamically distributes the load among the servers according to client requests. We define a \emph{CronJob}, a time-based scheduler, to periodically run the user-based balancer at specific time instants (to be specified by the HBase administrator). 
The user-based balancer is split into three phases. 

\subsubsection{Select Victim Regions}
The first phase consists in selecting victim regions according to the following: 
\begin{enumerate}
\item Compute, from the profiled information, the average response time per region. Let $ART=\{(R_1, art_1), \ldots, (R_n, art_n)\}$, where $art_i$ is the average respond time of region $R_i$.
\item Select the top $k$ victim regions $VRs$ with average response time greater than some threshold, $ART^{\mathtt{threshold}}$. $ART^{\mathtt{threshold}}$ is an input parameter of the algorithm to be specified by the HBase administrator depending on the clients' need and the specification of the cluster. Formally, $VRs = \big\{R_i \mid (R_i, art_i) \in ART \, \wedge \, art_i \geq ART^{\mathtt{threshold}}\big\}$.
\end{enumerate}

\subsubsection{Split Phase}
The second phase consists of splitting victim regions with respect to some keys. A region has to be split so that the requests would be dispatched over the two new regions. That is, if we have a massive number of requests on two keys $k_1$ and $k_2$ in some region, where the two keys belong to the first part of the region. Thus, it is not desirable to split the region in the middle, since it will remain congested. For this, we split the region with respect to a \emph{balance key} so that after the split, the future requests would be eventually dispatched between the two new regions. Note that small regions should not be split to avoid explosion of regions, but they should be moved to another less loaded region server, which will be discussed in the next phase. For this, we only split the region if its size is greater than $RS^\mathtt{threshold}$.
For every victim region $vr \in VRs$ where its size is greater than $RS^\mathtt{threshold}$, the split phase is defined as follows: 
\begin{enumerate}
\item Let $(k_1, r_1), \ldots, (k_\alpha, r_\alpha)$ be the number of requests, sorted with respect to the keys in the victim region $vr$.
\item We define the balance key to be equal to one if $\alpha$ is equal to one (i.e., only one key is requested by this region). Otherwise, we define the balance key, $bk$, so that after the split the requests would be split into the two new regions. For this, we first select the first key, $k_\beta$, from the requested keys satisfying the following property. The number of requests to the keys less than or equal to $k_\beta$ would be greater than the number of requests to the keys greater than $k_\beta$. Then, we set the balance key to be between $k_\beta$ and $k_{\beta + 1}$. Formally, $bk = \frac{k_\beta + k_{\beta + 1}}{2}$, where $\beta$ is the index of the key such that $\sum_{i=1}^{i=\beta}r_i \leq r^{av}$ and $\sum_{i=1}^{i=\beta + 1}r_i > r^{av}$ and $r^{av} = \frac{\sum_{i=1}^{i=\alpha}r_i}{2}$. 
\end{enumerate}

\subsubsection{Move Phase}
After the split phase some region servers may have many regions. For this, for every region server that corresponds to some victim regions, we count its number of regions. If the number of regions is greater than some threshold, $NR^{\mathtt{threshold}}$, we move the extra regions to other region servers with minimum number of regions and less than $NR^{\mathtt{threshold}}$. 
$NR^{\mathtt{threshold}}$ is an input parameter of our algorithm, which depends on the capacity of region servers. 

\subsection{General Algorithm}
The general structure of the algorithm of the user-balancer is depicted in Listing~\ref{code:user-balancer-algo}. It mainly consists of three phases: 
\begin{enumerate}
\item Select victim regions.
\item Splitting of victim regions. 
\item Moving of regions from high-loaded region servers. 
\end{enumerate}

\begin{lstlisting}[language=java,caption={User-Based Balancer Algorithm},label={code:user-balancer-algo}]
/*** Select Victim Regions ***/
ART = averageRTPerRegion();
VRs = filterVitimRegions(ART);

/*** Split Phase ***/
for(vr: VRs) {
   if( size(vr) < RS_THRESHOLD ) continue;
   TopKKeys = getTopKKeys(vr);
   if(s ize(TokKKeys) == 1 ) {
      balanceKey = middleKey(vr);  
   } else {
      requestAverage = sumRequests(TokKKeys);
      betaIndex = computeBetaIndex(TopKKeys);
      balanceKey = (TopKKeys[betaIndex] + 
                    TopKKeys[betaIndex + 1]) / 2;
   }
   split(vr, balanceKey);
}

/*** Move Phase ***/
for(vr: VRs) {
   RS = regionServer(vr);
   if( numberRegions(RS) <= NR_THRESHOLD ) continue;
   count = numberRegions(RS) - NR_THRESHOLD;
   RM = selectRegionsToMove();
   move(count, vr, RM);
}
\end{lstlisting}

\paragraph{Discussion}
In case where the average response time is very high (i.e., greater than $ART^{\mathtt{threshold}}$), the size of the region is smaller than $RS^\mathtt{threshold}$, and the other region servers contain more regions than $NR^{\mathtt{threshold}}$, the performance of the HBase cluster is not compatible with respect to user needs, and hence region servers have to be replaced with more powerful machines to satisfy the users' needs. 
